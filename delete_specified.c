#include<stdio.h>
int main()
{
    int i,n,a[10],pos;
    printf("Enter the number of elements of the array\n");
    scanf("%d",&n);
    printf("Enter the elements of the array: ");
    for(i=0; i<n; i++)
    {
        scanf("%d",&a[i]);
    }
    printf("Enter the position of the element\n");
    scanf("%d",&pos);
    for(i=pos; i<n-1; i++)
    {
        a[i]=a[i+1];
    }
    n=n-1;
    printf("The array after deleting the element is :\n");
    for(i=0; i<n; i++)
    {
        printf("%d",a[i]);
    }
    return 0;
}