#include<stdio.h>
int main()
{
   int m,n,i,j,matrix[20][20],transpose[20][20];
   printf("Enter the number of rows and columns: ");
   scanf("%d %d",&m,&n);
   printf("Enter the elements of the matrix:\n");
   for(i=0;i<m;i++)
   {
        for(j=0;j<n;j++)
        {
            scanf("%d",&matrix[i][j]);
        }
   }
   for(i=0;i<m;i++)
   {
        for(j=0;j<n;j++)
        {
            transpose[j][i]=matrix[i][j];
        }
   }
    printf("The transpose of the matrix is:\n");
    for(i=0;i<m;i++)
    {
        for(j=0;j<n;j++)
        {
        printf("%d\n",transpose[j][i]);
        }
    }
    return 0;
}